class AudioController {
    constructor() {
        this.bgMusic = new Audio('Assets/audio/bgmusic.mp3');
        this.flipSound = new Audio ('Assets/Audio/flip.mp3');
        this.matchSound = new Audio ('Assets/Audio/matched.mp3');
        this.victorySound = new Audio ('Assets/Audio/victory.mp3');
        this.gameOverSound = new Audio ('Assets/Audio/victory.mp3');
        this.bgMusic.loop = true;
        this.bgMusic.volume = 0.2;

    }
    
    startMusic() {
        this.bgMusic.play();
    }
    stopMusic() {
        this.bgMusic.pause();
        this.bgMusic.currentTime=0;
    }
    flip() {
        this.flipSound.play();
    }
    match() {
        this.matchSound.play();
    }
    victory() {
        this.stopMusic();
        this.victorySound.play();
    }
    gameOver() {
        this.stopMusic();
        this.gameOverSound.play();
    }
}

class SimplonBallZ {
    constructor(totalTime,cards) {
        
    }
}

function ready () {

    let overlays = Array.from(document.getElementsByClassName('overlay-text'));
    let cards = Array.from(document.getElementsByClassName('card'));

    overlays.forEach(overlay => {
        overlay.addEventListener('click', () => {
            overlay.classList.remove('visible');
            // game.startGame();
            let audioController = new AudioController();
audioController.startMusic();
        });
    });
    cards.forEach( card => {
        card.addEventListener('click', () => {
            // game flipecard(card);
        })
    })
}

if(document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', ready ());
} else {
    ready();
}

